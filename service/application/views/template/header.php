<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo $meta_title; ?></title>
  <?php echo $metatag; ?>
  <link rel="shortcut icon" href="<?php echo $url['base_url_static'] ?>img/ico.png" />
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
   <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	
	<link rel="stylesheet" href="<?php echo $url['base_url_static'];?>css/vendor/bootstrap.css">
	<link rel="stylesheet" href="<?php echo $url['base_url_static'];?>css/vendor/normalize.css">
	<link rel="stylesheet" href="<?php echo $url['base_url_static'];?>css/screen.css">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="<?php echo $url['base_url_static'];?>js/vendor/html5shiv.js"></script>
      <script src="<?php echo $url['base_url_static'];?>js/vendor/respond.min.js"></script>
    <![endif]-->
    <script src="<?php echo $url['base_url_static'];?>js/vendor/jquery-1.11.1.min.js"></script>

	
</head>
<body class="<?php echo $class_body; ?>">