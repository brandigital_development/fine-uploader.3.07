<nav class="navbar navbar-inverse" id="menuMain">
  <div class="container">

    <div class="navbar-header">

      <a  href="<?php echo $url['base_url'];?>" role="button" class="btn btn-top btn-volver col-xs-2 visible-xs">
        <!-- <img src="<?php echo $url['base_url_static'];?>img/buttons/volver.png" alt=""> -->
        <span>VOLVER</span>
      </a>

      <figure class="text-center visible-xs col-xs-8 logo">
        <img src="<?php echo $url['base_url_static'];?>img/logo-grimbergen.png" alt="" class="img-responsive logo-peregrino">
      </figure>

      <button type="button" class="navbar-toggle btn btn-top btn-menu col-xs-2" data-toggle="collapse" data-target=".navbar-collapse">
        <!-- <img src="<?php echo $url['base_url_static'];?>img/buttons/menu.png" alt=""> -->
        <span>MENU</span>
      </button>

    </div>

    <div class="navbar-collapse collapse sombraTexto">
      <ul class="nav navbar-nav col-sm-12 col-lg-9">
        <li class="home"><a href="<?php echo $url['base_url'];?>">HOME</a></li>
        <li class="bares"  style="display:none"><a href="bares">MAPA DE BARES</a></li>
        <li class="subir"><a href="participar">SUBIR FOTO</a></li>
        <li class="cerrar"><a href="login/logout">CERRAR SESIÓN</a></li>
      </ul>

      <div class="navbar-form navbar-right nombre col-sm-12 col-lg-3 hidden-xs">
        ¡Hola <span><?php echo $paramUser['usuarioSession']['PER_NOMBRE'];?>!</span>
      </div>
    </div><!--/.navbar-collapse -->

    <div class="row">
      <div class="col-xs-12 nombre visible-xs">
       ¡Hola <span><?php echo $paramUser['usuarioSession']['PER_NOMBRE'];?>!</span>
      </div>
    </div>

  </div>
</nav>