<footer class="container-fluid footer-container" id="menuFooter">
	<nav class="container">
		<div class="row-fluid">
			<ul class="nav navbar-nav col-md-12 col-lg-10">
			  <li><a href="static/pdf/AVISO_LEGAL.pdf" target="_blank">AVISO LEGAL</a></li>
			  <li><a href="static/pdf/condiciones_generales.pdf" target="_blank">CONDICIONES GENERALES</a></li>
			  <li><a href="static/pdf/politica_de_privacidad.pdf" target="_blank">POLÍTICA DE PRIVACIDAD</a></li>
			  <li><a href="http://sac.grimbergen.es/" target="_blank">CONTACTA</a></li>
			</ul>
			<figure class="col-xs-12 col-lg-2 logo">
				<img src="<?php echo $url['base_url_static'];?>img/logo-grimbergen.png" alt="Grimbergen" class="img-responsive">
			</figure>
		</div>
	</nav>
</footer>

<script type="text/javascript" charset="utf8" src="<?php echo $url['base_url_static'];?>js/vendor/bootstrap.min.js"></script>
<script type="text/javascript" charset="utf8" src="<?php echo $url['base_url_static'];?>js/vendor/placeholders.min.js"></script>

<script src="<?php echo $url['base_url_static'];?>js/script.js"></script>

<script>
	(function() {
	  if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
	    var msViewportStyle = document.createElement("style");
	    msViewportStyle.appendChild(
	      document.createTextNode("@-ms-viewport{width:auto!important}")
	    );
	    document.getElementsByTagName("head")[0].appendChild(msViewportStyle);
	  }
	})();
</script>
<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-55867438-1', 'auto');
		  ga('send', 'pageview');

</script>
</body>
</html>

	