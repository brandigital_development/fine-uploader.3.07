
<nav class="navbar navbar-inverse" id="menuMapa">
  <div class="container">

    <div class="navbar-header">
      
      <a  href="<?php echo $url['base_url'];?>" role="button"  class="btn btn-top btn-volver col-xs-2 visible-xs">
        <!-- <img src="<?php echo $url['base_url_static'];?>img/buttons/volver.png" alt=""> -->

        <span>VOLVER</span>
      </a>    

      <figure class="text-center visible-xs col-xs-6 logo">
        <img src="<?php echo $url['base_url_static'];?>img/logo-grimbergen.png" alt="" class="img-responsive logo-peregrino">
      </figure>

      <div class="col-xs-8" id="wrapperBusca" style="display:none;">
        <form action="">
          <input type="text" class="form-control sombraTexto campoTexto" id="inputPoblacion" placeholder="Buscar población">
        </form>
      </div>

      <button type="button" class="visible-xs navbar-toggle btn btn-top btn-cerca col-xs-2" >
        <!-- <img src="<?php echo $url['base_url_static'];?>img/buttons/brujula.png" alt=""> -->
        <span>CERCA DE MI</span>
      </button>
      
      <button type="button" class="visible-xs navbar-toggle btn-top btn-buscar col-xs-2" id="Btn-buscarBar">
        <!-- <img src="<?php echo $url['base_url_static'];?>img/buttons/zoom.png" alt=""> -->
        <span>BUSCAR</span>
      </button>
      
    </div>

    <div class="navbar-collapse collapse sombraTexto">
      <ul class="hidden-xs nav navbar-nav col-sm-12 col-lg-9">
        <li class="home"><a href="<?php echo $url['base_url'];?>">HOME</a></li>
        <li class="bares"><a href="bares">MAPA DE BARES</a></li>
        <li class="subir"><a href="participar">SUBIR FOTO</a></li>
          <?php if(isset($paramUser['usuarioSession']['PER_NOMBRE'])): ?>
            <li class="cerrar"><a href="login/logout">CERRAR SESIÓN</a></li>
          <?php endif ?>
      </ul>

      <ul class="nav navbar-nav visible-xs">
        <li class="home"><input type="text"></li>
      </ul>

      <div class="navbar-form navbar-right nombre col-sm-12 col-lg-3 hidden-xs">
          <?php if(isset($paramUser['usuarioSession']['PER_NOMBRE'])): ?>
              ¡Hola <span><?php   echo $paramUser['usuarioSession']['PER_NOMBRE'];  ?>!</span>
          <?php endif ?>
      </div>
    </div><!--/.navbar-collapse -->

    <div class="row">
      <div class="col-xs-12">
        <span class="si-icon si-icon-hamburger-cross" data-icon-name="hamburgerCross"></span>
      </div>
    </div>


  </div>
</nav>