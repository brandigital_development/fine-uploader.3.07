
    <section class="container-fluid" id="Main">
      <div class="container">
          <div class="row">
              <header class="col-lg-6 wrapperLeft">
                <figure class="text-center">
                  <img src="<?php echo $url['base_url_static'];?>img/logo-peregrino.png" alt="" class="img-responsive logo-peregrino">
                </figure>
                <div class="row wrapperBotellas">
                  <div class="col-xs-12 col-sm-8 col-md-12 col-md-offset-0 col-sm-offset-2">
                    <figure class="col-xs-4 text-right">
                      <img src="<?php echo $url['base_url_static'];?>img/blonde.png" alt="" class="img-responsive">
                    </figure>
                    <figure class="col-xs-4 text-center">
                      <img src="<?php echo $url['base_url_static'];?>img/double.png" alt="" class="img-responsive">
                    </figure>
                    <figure class="col-xs-4 text-left">
                      <img src="<?php echo $url['base_url_static'];?>img/blanche.png" alt="" class="img-responsive">
                    </figure>
                  </div>
                </div>
              </header>
              <article class="col-lg-6 wrapperRight">
                <h2 class="text-center sombraTexto">GANA UN VIAJE A LA ABADÍA<br>DE GRIMBERGEN.</h2>             
                <div class="row">
                  <!--div class="col-xs-12 col-xs-offset-1 col-sm-6 col-sm-offset-3">
                    <a href="login/oauth" class="btn btn-block btn-lg sombraTexto" role="button">CONÉCTATE</a>
                  </div-->

                  <div class="col-xs-12 col-xs-offset-1 col-sm-6 col-sm-offset-3">
                    <button type="button" id="btn-conectate" class="btn btn-block btn-lg sombraTexto">CONÉCTATE</button> 
                  </div>
                  <div class="col-xs-12 col-sm-6" style="display:none">
                    <button type="button" class="btn btn-block btn-lg sombraTexto">MAPA DE BARES</button>
                  </div>
                </div>

                <div class="row sombra-lg">
                  <figure class="col-xs-12">
                    <img src="<?php echo $url['base_url_static'];?>img/sombra-large.png" alt="" class="img-responsive">
                  </figure>
                </div>
              <div class="row">
                <div class="col-xs-12 text-center">
                  <h4>¿Cómo Participar?</h4>
                </div>
                  <figure class="col-xs-12 visible-sm text-center">
                    <a href="#comoParticipar"><img src="<?php echo $url['base_url_static'];?>img/icon-arrow.png" alt="" class="iconVer"></a>
                  </figure>
                <div class="col-xs-12" id="comoParticipar">
                  <ul class="wrapComo">
                    <li>
                      <span class="numero">1</span>
                      <p>Pide el pasaporte del peregrino en los bares de la promoción.</p>
                    </li>
                    <li>
                      <span class="numero">2</span>
                      <p>Disfruta de una Grimbergen de cada variedad* y reclama el sello de cada una. </p>
                    </li>
                    <li>
                      <span class="numero">3</span>
                      <p>Regístrate y sube una foto de tu pasaporte en la que se vean los 3 credenciales.</p>
                    </li>
                  </ul>
                </div>
              </div>
              <footer class="row sombraTexto">
                <div class="col-xs-1 col-sm-2 asterisco">*</div>
                <div class="hidden-xs col-sm-8 text-center">
                  <p>Las tres variedades deben ser consumidas<br>en el mismo establecimiento.</p>
                </div>
                <div class="visible-xs col-xs-10 text-center">
                  <p>Las tres variedades deben ser consumidas en el mismo establecimiento.</p>
                </div>
                <div class="col-xs-1 col-sm-2 asterisco">*</div>                
              </footer>
                <div class="row sombra-lg">
                  <figure class="col-xs-12">
                    <img src="<?php echo $url['base_url_static'];?>img/sombra-large.png" alt="" class="img-responsive">
                  </figure>
                </div>
              </article>
          </div>
      </div>
    </section> 