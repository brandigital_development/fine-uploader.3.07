<link rel="stylesheet" type="text/css" href="<?php echo $url['base_url_static'];?>css/validation-Engine-2.6.1/validationEngine.jquery.css">

<script type="text/javascript">
               var base_url_static ="<?php echo $url['base_url_static'];?>";   
               var base_url ="<?php echo $url['base_url'];?>";    
               var base_url_service ="<?php echo $url['base_url_service'];?>";                     

               var appgimbergen = {
                  data:<?php echo $paramUser['jsonParam']; ?>}; 
                 
</script>


 <section class="container-fluid" id="MainMenu">
      <div class="container">
          <div class="row">
              <header class="col-lg-6 wrapperLeftIn hidden-xs">
                <figure class="text-center">
                  <img src="<?php echo $url['base_url_static'];?>img/logo-peregrino.png" alt="" class="img-responsive logo-peregrino">
                </figure>
                <div class="row wrapperBotellas">
                  <figure class="col-xs-4 text-right">
                    <img src="<?php echo $url['base_url_static'];?>img/blonde.png" alt="" class="img-responsive">
                  </figure>
                  <figure class="col-xs-4 text-center">
                    <img src="<?php echo $url['base_url_static'];?>img/double.png" alt="" class="img-responsive">
                  </figure>
                  <figure class="col-xs-4 text-left">
                    <img src="<?php echo $url['base_url_static'];?>img/blanche.png" alt="" class="img-responsive">
                  </figure>
                </div>
              </header>

                    <article class="col-lg-6 wrapperRightIn" id="artCodigo" style="display:none">
                            <h4 class="text-center sombraTexto">
                                  Para completar tu participación, escribe la población y el nombre del bar
                                  en EL que estás disfrutando tu cerveza Grimbergen!
                            </h4>
                            <form role="form" id="formUpload" method="post" name="formUpload">
                              <div class="wrapperForm">
                                <div class="form-group">
                                      <input type="text" class="form-control sombraTexto campoTexto  validate[required]" id="inputPoblacion" placeholder="Población" maxlength="50">
                                </div>
                                <div class="form-group">
                                      <input type="text" class="form-control sombraTexto campoTexto  validate[required]" id="inputBar" placeholder="Nombre del Bar" maxlength="50">
                                </div>
                                <div class="form-group">
                                     <input type="text" class="form-control sombraTexto campoTexto  validate[required]" id="inputPasaporte" placeholder="* Número de Pasaporte" maxlength="12">
                                </div>
                                <div class="checkbox">
                                    <input type="checkbox" id="checkByC" name="checkByC" class="checkbox">
                                    <label for="checkByC">
                                      <p class="alerta">He leído y acepto las <a href="static/pdf/condiciones_generales.pdf" target="_blank">Condiciones Generales</a>, <a href="static/pdf/politica_de_privacidad.pdf" target="_blank">Política de Privacidad</a> y <a href="static/pdf/Grimbergen_Consumidor.pdf" target="_blank">Bases Legales</a>.</p>
                                    </label>
                                    <div id="info_checkbox" class="error"></div>

                                </div>
                              </div>
                              <div class="sombra"></div>
                              <div class="row wrapperButton">
                                <div class="col-xs-2 col-sm-1 asteriscoSmall">*</div>
                                <div class="col-xs-8 col-sm-6 pasaporte">
                                  <p class="alerta">El número de tu pasaporte está en la esquina superior derecha.</p>
                                </div>
                                <div class="col-xs-2 visible-xs asteriscoSmall text-right">*</div>
                                <div class="col-sm-5">
                                  <button type="submit" class="btn btn-block btn-lg" id="formUpload-submit">ACEPTAR</button>
                                </div>              
                              </div>
                            </form>

                          <figure class="row hidden-xs">
                            <div class="col-xs-12">
                              <img src="<?php echo $url['base_url_static'];?>img/map/sombra-map.png" class="img-responsive" alt="">
                            </div>              
                          </figure>

                    </article>


                    <article class="col-lg-5 wrapperRightIn" id="artImagen">
                        <h4 class="text-center sombraTexto">SUBE UNA FOTO DE TU<br>PASAPORTE CON LOS TRES<br>SELLOS PARA PARTICIPAR</h4>
                            <figure class="text-center" id="picUpload">                              
                                <div id="user-carga">
                                     <img src="<?php echo $url['base_url_static'];?>img/icon-upload.png" alt="" class="iconUpload">
                                     <img id="pre-imagen" src="<?php echo $url['base_url_static'];?>img/bg-upload.jpg" alt="" class="imgUpload img-responsive">
                                </div>
                            </figure>
                            <div class="row wrapperBtn">
                              <div class="col-xs-6 col-sm-6">                                
                                <div id="thumbnail-fine-uploader" class="btn btn-block btn-lg sombraTexto"></div>   
                              </div>
                            <div class="col-xs-6 col-sm-6" id="content-btn-subir" style="display:none">
                                <button id="btn-subir" type="button" class="btn btn-block btn-lg sombraTexto">SUBIR</button>
                              </div>
                            </div>
                            <div class="row sombra-lg">
                              <figure class="col-xs-12">
                                <img src="<?php echo $url['base_url_static'];?>img/sombra-large.png" alt="" class="img-responsive">
                              </figure>
                            </div>
                  </article>


                  <article class="col-lg-6 wrapperRightIn" id="artGracias"  style="display:none">
                        <h1 class="text-center sombraTexto">¡Gracias por<br>participar!</h1>

                        <div class="row text-center">
                          <div class="col-xs-12">
                            <p>
                              Estás a un paso de<br>
                              la abadía de Grimbergen
                              <br><br>
                              Sigue participando para<br>
                              tener más posibilidades<br>
                              de ganar el viaje.<br>
                              Puedes hacerlo hasta<br>1 vez al día.
                            </p>
                          </div>
                          <div class="form-group col-sm-4 col-sm-offset-4">
                            <a href="<?php echo $url['base_url'];?>" class="btn btn-lg sombraTexto btn-block aceptar">ACEPTAR</a>
                          </div>
                        </div>

                        <figure class="row sombra-lg">
                          <div class="col-xs-12">
                            <img src="static/img/map/sombra-map.png" class="img-responsive" alt="">
                          </div>              
                        </figure>
                </article>


          </div>
      </div>
    </section>

    <script type="text/javascript" src="<?php echo $url['base_url_static'];?>js/validation-Engine-2.6.1/languages/jquery.validationEngine-es.js"></script>
    <script type="text/javascript" src="<?php echo $url['base_url_static'];?>js/validation-Engine-2.6.1/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="<?php echo $url['base_url_static'];?>js/internas/loading.js"></script>
    <script type="text/javascript" src="<?php echo $url['base_url_static'];?>/js/vendor/nailthumb/jquery.nailthumb.1.1.js"></script> 
    <link rel="stylesheet" href="<?php echo $url['base_url_static'];?>js/vendor/upload/fineuploader/fineuploader-3.7.0.css">
    <script type="text/javascript" src="<?php echo $url['base_url_static'];?>js/vendor/upload/fineuploader/fineuploader-3.7.0.min.js"></script>


    <script type="text/javascript" src="<?php echo $url['base_url_static'];?>js/internas/participar.js"></script>
