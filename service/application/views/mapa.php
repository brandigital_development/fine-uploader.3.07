<section class="container-fluid" id="MainMenu">
      <div class="container">
          <div class="row">
              <header class="col-lg-6 wrapperLeftIn hidden-xs">
                <figure class="text-center hidden-xs">
                  <img src="<?php echo $url['base_url_static'];?>img/logo-peregrino.png" alt="" class="img-responsive logo-peregrino">
                </figure>
                <div class="row wrapperBotellas hidden-xs">
                  <figure class="col-xs-4 text-right">
                    <img src="<?php echo $url['base_url_static'];?>img/blonde.png" alt="" class="img-responsive">
                  </figure>
                  <figure class="col-xs-4 text-center">
                    <img src="<?php echo $url['base_url_static'];?>img/double.png" alt="" class="img-responsive">
                  </figure>
                  <figure class="col-xs-4 text-left">
                    <img src="<?php echo $url['base_url_static'];?>img/blanche.png" alt="" class="img-responsive">
                  </figure>
                </div>
              </header>
              <article class="col-lg-6 wrapperRightIn">
                <h4 class="text-center sombraTexto hidden-xs">Encuentra aquí el bar más<br>cercano para tomar tu<br>Grimbergen y encontrarte<br>con más peregrinos.</h4>
          <form role="form" id="formMapa">
            <div class="row hidden-xs">
              <div class="form-group col-sm-8">
                <input type="text" class="form-control sombraTexto campoTexto" id="inputPoblacion" placeholder="Buscar población">
              </div>
              <div class="col-sm-2">
                <button type="button" class="btn btn-block btn-lg"><img src="<?php echo $url['base_url_static'];?>img/buttons/zoom.png" alt=""></button>
              </div>
              <div class="col-sm-2">
                <button type="button" class="btn btn-block btn-lg"><img src="<?php echo $url['base_url_static'];?>img/buttons/brujula.png" alt=""></button>
              </div>
            </div>
            <div class="row text-center" id="gMap">
              <section class="wapperInfo text-left" style="left:40%; top:30%">
                <header>
                  <span class="titulo">Nombre del Bar</span>
                  <span class="direccion">Dirección</span>
                  <span class="telefono">Teléfono</span>
                  <span class="web">Web</span>
                  <span class="mail">Mail</span>
                </header>
              </section>
              <div class="icon-bar" style="left:20%; top:10%"></div>
              <div class="icon-lugar" style="left:30%; top:20%"></div>

              <!-- ACÁ IRÍA EL MAPA -->
              <figure class="col-xs-12 text-center">
                    <div id='map_canvas_cerca' style='height:500px; width: 100%'></div>
              </figure>
              <!-- ´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´ -->

            </div>
          </form>
                <div class="row sombra-lg hidden-xs">
                  <figure class="col-xs-12">
                    <img src="<?php echo $url['base_url_static'];?>img/sombra-large.png" alt="" class="img-responsive">
                  </figure>
                </div>
              </article>
          </div>
      </div>
    </section>