<!-- MODAL EDAD -->
	<section class="modal fade" id="modalEdad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <header class="modal-header text-center">	        
	        <h1 class="modal-title sombraTexto" id="myModalLabel">¿ERES MAYOR DE EDAD?</h1>
	      </header>
	      <div class="sombra"></div>
	      <article class="modal-body row">
	      	<div class="col-sm-6">
	      		<a href="#" id="btn-mayor-edad" class="btn btn-lg btn-block">SI</a>	
	      	</div>
	      	<div class="col-sm-6">
	      		<a href="#" class="btn btn-lg btn-block" data-dismiss="modal" data-toggle="modal" data-target="#modalAtencion" data-backdrop="static">NO</a>
	      	</div>	        
	      </article>
	      <footer class="modal-footer">
	        <p>
	        	Utilizamos cookies propias y de terceros para mejorar nuestros servicios, realizar análisis estadísticos sobre los hábitos de navegación de nuestros usuarios para mostrarles publicidad relacionada con sus preferencias, y facilitar la interacción con redes sociales. Si continúa navegando, consideramos que acepta su uso. Puede cambiar la configuración u obtener más información <a href="static/pdf/Cookies.pdf" target="_blank">aquí</a>. 
	        </p>
	      </footer>
	    </div>
	  </div>
	</section>

	<!-- MODAL ATENCION -->
	<section class="modal fade" id="modalAtencion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <header class="modal-header text-center">
	        <h1 class="modal-title sombraTexto" id="myModalLabel">ATENCIÓN</h1>
	      </header>
	      <div class="sombra"></div>
	      <article class="modal-body sombraTexto text-center">
	      	<h5>Para acceder a esta web necesitas ser mayor de 18 años.</h5>	        
	      </article>
	      <div class="sombra"></div>
<!-- 	      <footer class="modal-footer text-center">
	      	<div class="col-sm-6 col-sm-offset-3">
	      		<a href="#" class="btn btn-lg btn-block">ACEPTAR</a>	
	      	</div>
	      </footer> -->
	    </div>
	  </div>
	</section>
	<script type="text/javascript">
		      $(document).ready(function(){
		           <?php if($this->session->userdata('mayorEdad')!=true): ?>
		                $('#modalEdad').modal({
		                    keyboard: false,
		                    backdrop: 'static'
		                })
		           <?php endif ?>
		      });
    </script>