
	<!-- MODAL BROWSERS -->
	<section class="modal fade" id="modalBrowsers" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <header class="modal-header text-center">
	        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	        <h1 class="modal-title sombraTexto" id="myModalLabel">ATENCIÓN</h1>
	      </header>
	      <div class="sombra"></div>
	      <article class="modal-body sombraTexto text-center">
	      	<h5>El navegador que estás utilizando no es compatible con la funcionalidad.</h5>	        
	      </article>
			<ul class="nav nav-pills nav-stacked">
				<li class="active sombraTexto">
					<span class="numero">1</span>
					<p>Sal del navegador</p>
				</li>
				<li class="active sombraTexto">
					<span class="numero">2</span>
					<p>Desde la cámara del dispositivo haz la foto.</p>
				</li>
				<li class="active sombraTexto">
					<span class="numero">3</span>
					<p>Entra a la web de nuevo y elige la opción “Seleccionar desde galería”</p>
				</li>
			</ul>
	      <div class="sombra"></div>
	      <footer class="modal-footer text-center">
	      	<div class="col-sm-6 col-sm-offset-3">
	      		<a href="#" class="btn btn-lg btn-block">ACEPTAR</a>	
	      	</div>
	      </footer>
	    </div>
	  </div>
	</section>

	
