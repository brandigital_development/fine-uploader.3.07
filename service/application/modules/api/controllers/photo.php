<?php

if (!defined('BASEPATH'))
     exit('No direct script access allowed');

class Photo extends CI_Controller {

     function temp() { 
            $this->load->helper('return_json');
    
            if (empty($_FILES['qqfile'])) {
                 $response = array(
                                     "success"=>"false",
                                    "image" => array("name" => ''),                  
                                    "status" => array(
                                                "code" => '107'
                                                ,"descript" => 'DATOS_NO_VALIDOS_IMAGE_0'                            
                                                )
                        );
                     return_json($response);
                     return;
            }

              $pathOriginal = '../multimedia/tmp/';
              
              include(APPPATH . 'libraries/upload' . EXT);
              $handle = new upload($_FILES['qqfile']);
              if ($handle->uploaded) {
                  $name = uniqid('', true);

                  $handle->file_new_name_body = $name;
                  $handle->file_max_size = '5120000';                                      
                  $handle->mime_check = true;
                  $handle->allowed = array('image/*');
                  $handle->forbidden = array('application/*', 'audio/*', 'multipart/x-gzip', 'multipart/x-zip', 'text/plain', 'text/rtf', 'text/richtext', 'text/xml', 'video/*');
             //     $handle->image_resize = true;
             //     $handle->image_ratio_fill = true;
             //     $handle->image_x               = 564;
             //     $handle->image_y               = 317;
             //     $handle->image_background_color='#FFFFFF';

                  $handle->process($pathOriginal);
                  if ($handle->processed) {
                          if (!$handle->file_is_image) {
                                    $response = array(
                                       "success"=>"false",
                                        "image" => array("name" => ''),                  
                                        "status" => array(
                                                    "code" => '107'
                                                    ,"descript" => 'DATOS_NO_VALIDOS_IMAGE_1 '                            
                                                    )
                                    );                
                          }else{
                              $response = array(
                                       "success"=>"true",
                                        "image" => array("name" => $handle->file_dst_name),                  
                                        "status" => array(
                                                    "code" => '0'
                                                    ,"descript" => 'EXITO'                            
                                                    )
                                    );
                         }
                  }else{
                        $response = array(
                                    "success"=>"false",
                                    "image" => array("name" => ''),                  
                                    "status" => array(
                                                "code" => '107'
                                                ,"descript" => 'DATOS_NO_VALIDOS_IMAGE_2 '                            
                                                )
                        );
                  }
              } else {
                   $response = array(
                                     "success"=>"false",
                                    "image" => array("name" => ''),                  
                                    "status" => array(
                                                "code" => '107'
                                                ,"descript" => 'DATOS_NO_VALIDOS_IMAGE_3 '                            
                                                )
                        );

              }
           
              return_json($response,'text/plain');
              return;
    }


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */