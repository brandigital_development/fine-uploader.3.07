<?php

if (!function_exists('encode_registrado')) {

    function encryptuser_encode_registrado($IdRegistrado) {
        $CI = & get_instance();
        $CI->load->library('encrypt');
        $userEncript_expire = $CI->config->item('userEncript_expire');

        $arrUser = array("regid" => (int) $IdRegistrado
            , "expire" => time() + $userEncript_expire
        );

        $UserEncrypted = $CI->encrypt->encode(json_encode($arrUser));
        $UserEncrypted = rawurlencode($UserEncrypted);

        return $UserEncrypted;
    }

    function encryptuser_decode_registrado() {
      
        $CI = & get_instance();
        $CI->load->library('encrypt');
        $userEncript = $CI->input->post('hash');
        
        $userEncript=rawurldecode($userEncript);
        
        if (empty($userEncript)) {
            $data['code'] = '107';
            $data['descript'] = 'DATOS_NO_VALIDOS1';
            $CI->load->view('xml/xml_exit', $data);
            exit();
        }

        $userEncript_expire = $CI->config->item('userEncript_expire');
        $pDecodeString = $CI->encrypt->decode($userEncript);


        $pJsonUser = json_decode($pDecodeString, true);
        
        $idRegistrado = $pJsonUser['regid'];

        $timeExpire = $pJsonUser['expire'];

        $SegundosRestantes = $timeExpire - time();

        if ($SegundosRestantes <= 0) {
            //die('se quedo sin tiempo');
            $data['code'] = '666';
            $data['descript'] = 'SESSION_CADUCO';
            $CI->load->view('xml/xml_exit', $data);
            exit();
        }

        if (!is_int($idRegistrado)) {
            $data['code'] = '107';
            $data['descript'] = 'DATOS_NO_VALIDOS2';
            $CI->load->view('xml/xml_exit', $data);
            exit();
        }
        /*
          $data['code'] = '666';
          $data['descript'] = 'SESSION_CADUCO';
          $CI->load->view('xml/xml_exit',$data);
          return;
         */

        return $idRegistrado;
    }

    function encryptuser_getRegistrado($RegId) {
        /*
          RESPUESTA = 0;
          COMENTARIO = 'TODO OK';


          RESPUESTA = 102;
          COMENTARIO = 'EL REGISTRADO NO EXISTE O NO PERTENECE A LA ACCION COMERCIAL';
         */
        $CI = & get_instance();
        $CI->load->model('persona/registrado_model');
        $result = $CI->registrado_model->get($CI->config->item('ACO_ID'), $RegId, $CI->config->item('ORIGEN_FACEBOOK'));

        switch ($result['RESPUESTA']):
            case 0:
                return $result;
                break;
            case 102:
                return false;
                break;
            default:
                DIE('NO EXISTE LOG');
                return;
        endswitch;
    }

}