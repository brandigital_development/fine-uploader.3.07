<?php

if ( ! function_exists('get_app_data'))
{
  function get_app_data()
  {

          
      $CI = & get_instance();
            

      $fb_appid=$CI->config->item('FB_APPID');
      $fb_secret=$CI->config->item('FB_SECRET');


      $usuarioSession = $CI->session->userdata('usuarioSession');


      $status="0";
      $descripcion="nologin";
      $source='';
      if(!empty($usuarioSession)){
             $status="0";
             $descripcion="login";
             $source=array("first_name" => $usuarioSession['PER_NOMBRE']
                          ,"last_name"  => $usuarioSession['PER_APELLIDO']                            
                          ,"hash"       => $usuarioSession['PER_HASH']);
          

      }else{
          redirect('/', 'refresh');
          return;
      }

       $arrInfo = array(
                "status" => $status,
                "descripcion" => $descripcion, 
                "app"  => array(                             
                                "session" => $CI->csrf_protection_token->get_csrf_hash()
                                ,"user"=>$source
                                ,'cupon'=>array('imagen'    => ''
                                                ,'poblacion'=> ''
                                                ,'bar'      => ''
                                                ,'code'     => '')
                           
                      )
            );

        $salida['usuarioSession']=$usuarioSession;
        $salida['jsonParam']=json_encode($arrInfo);        
  
      return $salida;
        
  }

}
?>