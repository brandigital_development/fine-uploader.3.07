<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class csrf_protection_token {

    private $CI;
    
    protected $_csrf_session_name = 'cicsrftoken';
    protected $_csrf_token_name = 'ses';
    protected $_csrf_expire = '8280';
    protected $_csrf_hash = '';

    // -------------------------------------------------------------------------

    public function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->library('encrypt');

    }

    // -------------------------------------------------------------------------

    /**
     * Verify Cross Site Request Forgery Protection
     *
     * @return	object
     */
    public function csrf_verify() {
        
          if ($_SERVER['REQUEST_METHOD'] == 'POST'){
              // Is the token field set and valid?
             $posted_token = $this->CI->input->post($this->_csrf_token_name);
              
             if ($posted_token ===''){
                 // Invalid request, send error 400.
                 //show_error('Request was invalid. Tokens did not match.', 400);
                 $this->csrf_show_error();
             }

             if ($posted_token === FALSE){
                 // Invalid request, send error 400.
                 //show_error('Request was invalid. Tokens did not match.', 400);
                 $this->csrf_show_error();
             }

             $encriptString=rawurldecode($posted_token);

             if (empty($encriptString)) {
         			 $this->csrf_show_error();       	  
        	 }

             $decodeString = $this->CI->encrypt->decode($encriptString);

             $jsonString = json_decode($decodeString, true);

             if (!isset($jsonString['expire'])) {
         			 $this->csrf_show_error();       	  
        	  }

             $timeExpire = $jsonString['expire'];
 
          	 $SegundosRestantes = $timeExpire - time();


             if ($SegundosRestantes <= 0) {
               	$this->csrf_show_error();  
        	 }


          }else{
              $this->csrf_show_error();
          }
        //  return true;
    }

    public function get_csrf_hash() {
       
        $msg = $_SERVER['REMOTE_ADDR'] . time() . $_SERVER['SERVER_NAME'];

        $arrUser = array("msg" => $msg
   					    ,"expire" => time() + $this->_csrf_expire
     				  );

         //  $userEncript=rawurldecode($userEncript);

       $this->_csrf_hash  =$this->CI->encrypt->encode(json_encode($arrUser));

       $this->_csrf_hash =rawurlencode($this->_csrf_hash);
      
       return $this->_csrf_hash;
    }

    // -------------------------------------------------------------------------
    /**
     * Show CSRF Error
     *
     * @return	void
     */
    public function csrf_show_error() {
         $response = array(
                                          "success"=>"false",                                                     
                                          "status" => array(
                                                      "code" => '107'
                                                      ,"descript" => 'DATOS_CSRF_NO_VALIDOS'                                                               
                                                      )
                              );
                

          echo json_encode($response);
          exit; 
       }

      


}