<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class time_protection {

    private $CI;

    
    public function __construct() {
        $this->CI = & get_instance();
    }

    public function verify_cierre_time() {
        
        include(APPPATH.'config/closetime.php');
        
        $getservertime = $this->time_server();
        
        $timeserver = strtotime($getservertime);
        $close_time = strtotime($TIMECLOSE);

        $salida=false;
        if($timeserver > $close_time){
            $salida=true;
          //  die('error');
          //  $this->time_show_error();
        }
        return $salida;
    }

    public function time_server(){
        $timezone = date_default_timezone_get();
        include(APPPATH.'config/closetime.php');
        date_default_timezone_set($TIMEZONE_SERVER);
        
        $timeserver = date("d-m-Y H:i:s");
       
        return $timeserver;
    }
    
    public function time_show_error() {
        include(APPPATH.'config/closetime.php');
        if($URLREDIRECT==''){
            /*
            $data['code'] = '666';
            $data['descript'] = 'TIME_CADUCO';
            $this->CI->load->view('xml/xml_exit', $data);
            */
            
            return;
        }else{
           header('LOCATION: '.$URLREDIRECT);
           exit();
        }
    }

}